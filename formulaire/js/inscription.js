$(function () {

    $.validator.addMethod("PWCHECK",
        function(value,element){
            if( /^(?=.*?[A-Z]{1,})(?=(.*[a-z]){1,})(?=(.*[0-9]){1,})(?=(.*[$@$!%*?&]){1,}).{8,}$/.test(value)){
            return true;
        }else{
            return false;
            };
        }
    );

    $("#inscription_form").validate(
        {

            rules:{
                    nom_per:{
                        required : true,
                        minlength : 2,
                    },
                    prenom_per:{
                        required : true,
                        minlength : 2,
                    },
                    email_per:{
                        required : true,
                         email: true
                    },
                    password_per:{
                        required : true,
                        PWCHECK : true,

                    },
                    passwordConfirmation_per:{
                        required : true,
                        equalTo: "#password_per"
                    },

            },
            messages:{

                nom_per: {
                    required: "Veuillez saisir votre Nom",
                    minlength: "Votre nom doit être composé de 2 lettres au minimun"
                },
                prenom_per: {
                    required: "Veuillez saisir votre Prénom",
                    minlength: "Votre Prénom doit être composé de 2 lettres au minimun"
                },
                email_per: {
                    required: "Veuillez saisir votre email",
                    email: "Votre adresse e-mail doit avoir le format suivant : name@domain.com"
                },
                password_per: {
                    required: "Veuillez saisir votre mot de passe",
                    PWCHECK: "Votre mot de passe doit comporter au minimum 8 caractères, dont une minuscule, un caractère spécial, une majuscule et un chiffre"
                },
                passwordConfirmation_per: {
                    required: "Veuillez répéter votre mot de passe",
                    equalTo: "Les mots de passe ne sont pas identiques"
                },
            }
        }
    )
});