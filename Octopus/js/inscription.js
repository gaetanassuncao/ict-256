$(function () {

    $("#moniteur_per").change(function (){
        if(this.checked){
            $("#nombreeleves").css("display", "block");
        }else{
            $("#nombreeleves").css("display", "none");
        }
    });

    $("#club_per").hide();
    jQuery("#prive").attr('checked', true);

    $('#inscription_form input').on('change', function()
    {
        if($("[name=radioName]:checked").val() == 1)
        {
            $("#club_per").hide();
        }else if ($("[name=radioName]:checked").val() == 2) {
            $("#club_per").show();
        }
    });

    $("#inscription_form").validate(
        {
            rules:{
                nom_per: {
                    required: true,
                    minlength: 2
                },
                prenom_per: {
                    required: true,
                    minlength: 2
                },
                email_per: {
                    required: true,
                    email: true
                },
                nombre_per: {
                    required: true
                },
                telephone_per: {
                    required: true,
                    minlength : 4,
                    maxlength : 12
                },
                club_per:{
                    required : true,
                    minlength : 2
                },

            },
            messages:{
                nom_per: {
                    required: "Veuillez saisir votre nom",
                    minlength: "Votr nom doit être composé de deux caractères au minimum"
                },
                prenom_per: {
                    required: "Veuillez saisir votre prenom",
                    minlength: "Votre nom doit être composé de deux caractères au minimum"
                },
                email_per: {
                    required: "Veuillez saisir votre email",
                    email: "Votre email doit avoir le format suivant : name@domain.com"
                },
                nombre_per: {
                    required: " Veuillez saisir le nombres d'élèves"
                },
                telephone_per: {
                    required: "Veuillez saisir votre telephone"
                },
                club_per:{
                    required: "Veuillez saisir votre club/école",
                    minlength:"Votre club doit comporter minimun 2 lettres"

                }
            }
        }
    )
});
