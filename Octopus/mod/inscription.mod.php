<div class="modal fade" id="inscription_mod" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Inscription </h4>
            </div>
            <div class="modal-body">
                <form id="inscription_form" action="check.php" method="post">



                    <!--Nom-->
                    <div class="form-group row">
                        <label for="nom_per" class="col-sm-2 col-form-label">Nom :</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nom_per" name="nom_per" placeholder="Votre nom">
                        </div>
                    </div>

                    <!--Prénom-->
                    <div class="form-group row">
                        <label for="prenom_per" class="col-sm-2 col-form-label">Prenom :</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="prenom_per" name="prenom_per" placeholder="Votre prénom">
                        </div>
                    </div>

                    <!--Email-->
                    <div class="form-group row">
                        <label for="email_per" class="col-sm-2 col-form-label">Email :</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email_per" name="email_per" placeholder="Votre email">
                        </div>
                    </div>

                    <!--Téléphone-->
                    <div class="form-group row">
                        <label for="telephone_per" class="col-sm-2 col-form-label">Téléphone</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="telephone_per" name="telephone_per" placeholder="Votre Numero">
                        </div>
                    </div>

                    <!--Moniteur-->
                    <div class="form-group row">

                        <div class="col-sm-10">
                            <input type="checkbox" checked id="moniteur_per" name="moniteur_per">
                            <label for="nom_per"> Vous êtes moniteur ? </label>
                            <div id="nombreeleves">
                                <label for="nom_per"> Nombre d'élèves :  </label>
                                <input type="number" min="1" max="15" id="nombre_per" name="nombre_per">
                            </div>
                        </div>
                    </div>

                    <!-- Catégorie -->

                    <div class="form-group row">

                            <label for="categorie_per" class="col-sm-2 col-form-label" > Catégorie:  </label> <br>
                            <div class="col-sm-10">
                            <select type="text" id="plongeur" name="radiocategorie">
                                <option>Nageur</option>
                                <option>Plongeur</option>
                                <option>Apnéiste</option>
                            </select>
                            </div>
                    </div>


                    <!-- type -->

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <label class="col-sm-2 col-form-label">Type : </label>
                            <input type="radio" id="prive" name="radioName" value="1">
                            <label for="prive">Privé</label>
                            <input type="radio" id="club" name="radioName" value="2">
                            <label for="club">Club / école</label>
                            <input type="text" class="form-control" id="club_per" name="club_per" placeholder="Votre Club">
                        </div>
                    </div>



            </div>
            <div class="modal-footer">
                <!--Bouton submit-->
                <div class="form-group row">
                    <div class="col-md-offset-8 col-md-4">
                        <input type="submit" class="form-control btn btn-primary submit" id="submit_conf"
                               value="S'inscrire">
                    </div>
                </div>

                <!--Bouton reset-->
                <div class="form-group row">
                    <div class="col-md-offset-8 col-md-4">
                        <input type="reset" class="form-control btn btn-warning submit" id="reset_conf" value="Annuler">
                    </div>
                </div>
            </div>
            </form>

        </div>

    </div>
</div>

