<!DOCTYPE html>
<!-- saved from url=(0073)https://moodle.ceff.ch/pluginfile.php/22406/mod_page/content/3/index.html -->
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Cidisi - Games</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>


</head>
<body>
<div class="container">

    <!-- Zone de notification -->

    <div class="col-md-12">
        <div class="panel panel-primary">

            <div class="panel-heading">
                <h3>Réservations à venir</h3>
            </div>

            <div class="panel-body">

                <table class="table table-bordered table-striped table-condensed">
                    <tbody><tr>
                        <td>2019-09-07</td>
                        <td>Test</td>
                        <td>Junod Michel</td>
                        <td>3</td>
                        <td>
                            <a href="https://moodle.ceff.ch/pluginfile.php/22406/mod_page/content/3/details.php?id_res=5"><button class="btn btn-primary detail_ins" id_res="5">Détails</button></a>
                        </td>
                        <td>
                            <button class="btn btn-primary add_ins" data-toggle="modal" data-target="#inscription_mod" id_res="5">S'inscrire</button>

                        </td>
                    </tr>

                    <tr>
                        <td>2019-09-17</td>
                        <td></td>
                        <td>Junod Michel</td>
                        <td>0</td>
                        <td>
                            <a href="https://moodle.ceff.ch/pluginfile.php/22406/mod_page/content/3/details.php?id_res=2"><button class="btn btn-primary detail_ins" id_res="2">Détails</button></a>
                        </td>
                        <td>
                            <button class="btn btn-primary add_ins" data-toggle="modal" data-target="#inscription_mod" id_res="2">S'inscrire</button>

                        </td>
                    </tr>

                    <tr>
                        <td>2019-09-28</td>
                        <td></td>
                        <td>Junod Michel</td>
                        <td>3</td>
                        <td>
                            <a href="https://moodle.ceff.ch/pluginfile.php/22406/mod_page/content/3/details.php?id_res=7"><button class="btn btn-primary detail_ins" id_res="7">Détails</button></a>
                        </td>
                        <td>
                            <button class="btn btn-primary add_ins" data-toggle="modal" data-target="#inscription_mod" id_res="7">S'inscrire</button>
                        </td>
                    </tr>

                    <tr>
                        <td>2019-10-13</td>
                        <td>Entrainement</td>
                        <td>Junod Michel</td>
                        <td>15</td>
                        <td>
                            <a href="https://moodle.ceff.ch/pluginfile.php/22406/mod_page/content/3/details.php?id_res=6"><button class="btn btn-primary detail_ins" id_res="6">Détails</button></a>
                        </td>
                        <td>
                            Complet
                        </td>
                    </tr>

                    </tbody></table>



            </div>

            <div class="panel-footer">

            </div>

        </div>
    </div></div>
<?php
    Require_once("./mod/inscription.mod.php")
?>

</body>
<script src="./js/inscription.js"></script>
</html>